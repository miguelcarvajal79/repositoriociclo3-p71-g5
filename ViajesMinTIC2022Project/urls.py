"""ViajesMinTIC2022Project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from ViajesMinTIC2022App import views

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('ruta/', views.RutaView.as_view()),
    path('ruta/<int:pk>/', views.RutaView.as_view()),
    path('ruta/rutas/', views.RutaAllView.as_view()),
    path('rol/', views.RolDetailView.as_view()),
    path('rol/create/', views.RolCreatedView.as_view()),
    path('rol/del/<int:pk>/', views.RolDeleteView.as_view()),
    path('rol/up/<int:pk>/', views.RolUpdateView.as_view()),
    path('usuario/', views.UsuarioView.as_view()),
    path('usuario/<int:pk>', views.UsuarioView.as_view()),
    path('usuario/usuario/', views.UsuarioAllView.as_view()),
  
]
