from rest_framework import status, views
from rest_framework.response import Response
from ViajesMinTIC2022App.models.ruta import Ruta
from ViajesMinTIC2022App.serializers.rutaSerializer import CrearRutaSerializer, MostrarRutaSerializer

class RutaView(views.APIView):
    
    def get(self, request, pk):
        ruta = Ruta.objects.get(pk=pk)
        serializer = MostrarRutaSerializer(ruta)
        
        return Response(serializer.data, 200)

    def post(self, request, *args, **kwargs):
        serializer = MostrarRutaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
                
        return Response(serializer.data['id'], status=status.HTTP_201_CREATED)
    
    def delete(self, request, pk):
        try:
            ruta = Ruta.objects.get(pk=pk)
            ruta.delete()
            return Response({"Respuesta": "Ruta eliminada."}, 200)
        except:
            return Response({"Respuesta": "Ruta no existe."}, 400)
    
    def put(self, request, pk):
        try:
            ruta = Ruta.objects.get(pk=pk)
            serializer = MostrarRutaSerializer(ruta, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
                
            return Response({"Respuesta": "Ruta actualizada."}, 200)
        except:
            return Response({"Respuesta": "Ruta no existe"}, 400)
        
class RutaAllView(views.APIView):
    def get(self, request):
        listaRutas = Ruta.objects.all().order_by('id')
        serializer = MostrarRutaSerializer(listaRutas, many=True)
        return Response(serializer.data, 200)
