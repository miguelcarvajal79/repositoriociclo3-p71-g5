from rest_framework import status, views
from rest_framework.response import Response
from ViajesMinTIC2022App.models.usuario import Usuario
from ViajesMinTIC2022App.serializers.usuarioSerializer import CrearUsuarioSerializer, MostrarUsuarioSerializer

class UsuarioView(views.APIView):
    
    def get(self, request, pk):
        usuario = Usuario.objects.get(pk=pk)
        serializer = MostrarUsuarioSerializer(usuario)
        
        return Response(serializer.data, 200)

    def post(self, request, *args, **kwargs):
        serializer =  CrearUsuarioSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
                
        return Response(serializer.data['id'], status=status.HTTP_201_CREATED)
    
    def delete(self, request, pk):
        try:
            usuario = Usuario.objects.get(pk=pk)
            usuario.delete()
            return Response({"Respuesta": "Usuario eliminado."}, 200)
        except:
            return Response({"Respuesta": "Usuario no existe."}, 400)
    
    def put(self, request, pk):
        try:
            usuario = Usuario.objects.get(pk=pk)
            serializer = MostrarUsuarioSerializer(usuario, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
                
            return Response({"Respuesta": "Usuario actualizado."}, 200)
        except:
            return Response({"Respuesta": "Usuario no existe"}, 400)
        
class UsuarioAllView(views.APIView):
    def get(self, request):
        listaUsuarios = Usuario.objects.all().order_by('id')
        serializer = MostrarUsuarioSerializer(listaUsuarios, many=True)
        return Response(serializer.data, 200)
