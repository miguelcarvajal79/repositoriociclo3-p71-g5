


from django.views.generic.base import View
from rest_framework import generics, status 
from rest_framework import views
from rest_framework import response
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from ViajesMinTIC2022App.models.rol import Rol
from ViajesMinTIC2022App.serializers.rolSerializer import RolSerializer,CreateRolSerializer



class RolDetailView(generics.RetrieveAPIView):
  queryset = Rol.objects.all()
  serializer_class = RolSerializer

  def get(self, request):
       
       queryset = self.get_queryset()
       serializer_rol = RolSerializer(queryset, many=True)
       
       #return Response({"message": "Rol exist"})
       return Response(serializer_rol.data)

class RolCreatedView (generics.CreateAPIView):
   queryset = Rol.objects.all()
   serializer_class = RolSerializer   

   '''def post(self, request ):
        serializer_create = CreateRolSerializer(data=request.data)
        serializer_create.is_valid(raise_exception=True)
        serializer_create.save()
      
        return response({"message": "Rol created"}, status.HTTP_201_CREATED)'''

class RolDeleteView(generics.DestroyAPIView):
    
    def delete(self, request, pk):
        Roldel = Rol.objects.get(pk=pk)
        Roldel.delete()
        return Response({"message": "Rol Deleted"}, 200)

class RolUpdateView(generics.UpdateAPIView):
    queryset = Rol.objects.all()
    serializer_class = RolSerializer 

    '''def put(self,request,pk):
        Rolup = Rol.objects.get(pk=pk)
        serializer_put= RolSerializer(Rolup, data=request.data)
        serializer_put.is_valid(raise_exception=True)
        serializer_put.save()
        return response(200)'''




    




