from django.db import models

class Usuario   (models.Model):
    id = models.AutoField(primary_key=True)
    nombres = models.CharField("Nombres",max_length=30,null=False)
    apellidos = models.CharField("Apellidos",max_length=40,null=False)
    tipoId = models.CharField("Tipo",max_length=15,null=False)
    numeroId = models.CharField("Numero",max_length=15,null=False)
    celular = models.CharField("celular",max_length=10, null=False)
    email = models.CharField("celular",max_length=30,null=False)
    fechaNacimiento =models.DateField(null=False)
    rol = models.CharField("Rol",max_length=15,null=False)