from django.db import models

class Ruta(models.Model):
    id = models.AutoField(primary_key=True)
    origen = models.CharField("Origen",max_length=30,null=False)
    destino = models.CharField("Destino",max_length=30,null=False)
    fecha = models.DateField(null=False)
    distancia = models.DecimalField(max_digits=19,decimal_places=2,default=0)
    tiempo = models.DurationField(default=0)
    estado = models.CharField("Estado",max_length=1,default='A')