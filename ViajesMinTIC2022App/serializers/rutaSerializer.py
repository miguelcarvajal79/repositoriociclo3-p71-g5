from django.db.models import fields
from ViajesMinTIC2022App.models.ruta import Ruta
from rest_framework import serializers

class CrearRutaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ruta
        fields = ['origen','destino','fecha','distancia','tiempo','estado']
        
class MostrarRutaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ruta
        fields = ['id','origen','destino','fecha','distancia','tiempo','estado']