from ViajesMinTIC2022App.models.rol import Rol
from rest_framework import serializers

class RolSerializer (serializers.ModelSerializer):
    class Meta:
        model=Rol
        fields =['Id','Nombre','Estado']

class CreateRolSerializer (serializers.ModelSerializer):
    class Meta:
        model=Rol
        fields =['Nombre','Estado']
