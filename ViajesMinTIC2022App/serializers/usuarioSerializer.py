from django.db.models import fields
from ViajesMinTIC2022App.models.usuario import Usuario
from rest_framework import serializers

class CrearUsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['nombres','apellidos','tipoId','numeroId','celular','email','fechaNacimiento','rol']
        
class MostrarUsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['id','nombres','apellidos','tipoId','numeroId','celular','email','fechaNacimiento','rol']