from django.contrib import admin
from .models.ruta import Ruta
from .models.rol import Rol
from .models.usuario import Usuario

# Register your models here.
admin.site.register(Ruta)
admin.site.register(Rol)
admin.site.register(Usuario)