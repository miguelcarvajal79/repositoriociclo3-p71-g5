from django.apps import AppConfig


class Viajesmintic2022AppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ViajesMinTIC2022App'
